/* USER CODE BEGIN Header */
/**
 ******************************************************************************
 * @file           : main.c
 * @brief          : Main program body
 ******************************************************************************
 * @attention
 *
 * <h2><center>&copy; Copyright (c) 2021 STMicroelectronics.
 * All rights reserved.</center></h2>
 *
 * This software component is licensed by ST under Ultimate Liberty license
 * SLA0044, the "License"; You may not use this file except in compliance with
 * the License. You may obtain a copy of the License at:
 *                             www.st.com/SLA0044
 *
 ******************************************************************************
 */
/* USER CODE END Header */
/* Includes ------------------------------------------------------------------*/
#include "main.h"
#include "cmsis_os.h"

/* Private includes ----------------------------------------------------------*/
/* USER CODE BEGIN Includes */
#include "stm32746g_discovery_lcd.h"
#include "stm32746g_discovery_ts.h"
#include "stdio.h"
#include "homme.h"
#include "bitmap.h"
#include "pangolin.h"
#include "vaccin.h"
#include "virus.h"
#include "virus1.h"
//#include "virus1.h"
/* USER CODE END Includes */

/* Private typedef -----------------------------------------------------------*/
/* USER CODE BEGIN PTD */

/* USER CODE END PTD */

/* Private define ------------------------------------------------------------*/
/* USER CODE BEGIN PD */
/* USER CODE END PD */

/* Private macro -------------------------------------------------------------*/
/* USER CODE BEGIN PM */

/* USER CODE END PM */

/* Private variables ---------------------------------------------------------*/

DMA2D_HandleTypeDef hdma2d;

I2C_HandleTypeDef hi2c3;

LTDC_HandleTypeDef hltdc;

RNG_HandleTypeDef hrng;

RTC_HandleTypeDef hrtc;

TIM_HandleTypeDef htim1;

UART_HandleTypeDef huart1;

SDRAM_HandleTypeDef hsdram1;

osThreadId Game_OverHandle;
osThreadId PangolinHandle;
osThreadId HommeHandle;
osThreadId VirusHandle;
osThreadId VaccinHandle;
osThreadId VictoireHandle;
osThreadId HuminiteHandle;
osThreadId DemarrageHandle;
osMessageQId QueuePangolin_virusHandle;
osMessageQId QueueVirus_GammeOverHandle;
osMessageQId QueueUART_hommeHandle;
osMessageQId QueueHuminite_VictoireHandle;
osMessageQId QueueHomme_VirusHandle;
osMutexId myMutex_tp5Handle;
/* USER CODE BEGIN PV */

/* USER CODE END PV */

/* Private function prototypes -----------------------------------------------*/
void SystemClock_Config(void);
static void MX_GPIO_Init(void);
static void MX_I2C3_Init(void);
static void MX_LTDC_Init(void);
static void MX_RTC_Init(void);
static void MX_TIM1_Init(void);
static void MX_USART1_UART_Init(void);
static void MX_FMC_Init(void);
static void MX_DMA2D_Init(void);
static void MX_RNG_Init(void);
void Game_over(void const * argument);
void pangolain(void const * argument);
void homme(void const * argument);
void virus1(void const * argument);
void vaccin(void const * argument);
void victoire(void const * argument);
void huminite(void const * argument);
void demarrage(void const * argument);

/* USER CODE BEGIN PFP */

/* USER CODE END PFP */

/* Private user code ---------------------------------------------------------*/
/* USER CODE BEGIN 0 */
uint8_t rxbuffer = 0; // variable pour recevoir le caractère en réception de l’uart
uint32_t Xpangolain = 0, Ypangolain = 10;
uint32_t Xhomme = 0, Yhomme = 200;
uint32_t Xvirus1 = 0, Yvirus1 = 10;
uint32_t Xvaccin, Yvaccin = 100;
int8_t huminite_homme = 0;
/* USER CODE END 0 */

/**
  * @brief  The application entry point.
  * @retval int
  */
int main(void)
{
  /* USER CODE BEGIN 1 */

  /* USER CODE END 1 */

  /* MCU Configuration--------------------------------------------------------*/

  /* Reset of all peripherals, Initializes the Flash interface and the Systick. */
  HAL_Init();

  /* USER CODE BEGIN Init */

  /* USER CODE END Init */

  /* Configure the system clock */
  SystemClock_Config();

  /* USER CODE BEGIN SysInit */

  /* USER CODE END SysInit */

  /* Initialize all configured peripherals */
  MX_GPIO_Init();
  MX_I2C3_Init();
  MX_LTDC_Init();
  MX_RTC_Init();
  MX_TIM1_Init();
  MX_USART1_UART_Init();
  MX_FMC_Init();
  MX_DMA2D_Init();
  MX_RNG_Init();
  /* USER CODE BEGIN 2 */

	BSP_LCD_Init();
	BSP_LCD_LayerDefaultInit(0, LCD_FB_START_ADDRESS);
	BSP_LCD_LayerDefaultInit(1,
	LCD_FB_START_ADDRESS + BSP_LCD_GetXSize() * BSP_LCD_GetYSize() * 4);
	BSP_LCD_DisplayOn();
	BSP_LCD_SelectLayer(0);
	BSP_LCD_Clear(LCD_COLOR_BLUE); //effacer l'ecran par une couleur bleue
//	BSP_LCD_DrawBitmap(0, 0, (uint8_t*) image_fond_bleu_bmp);
	BSP_LCD_SelectLayer(1);
//	BSP_LCD_SelectLayer(00);
	BSP_LCD_Clear(0); //effacer l'ecran par une couleur bleue
	BSP_LCD_SetFont(&Font8); // taille de police =10
	BSP_LCD_SetTextColor(LCD_COLOR_WHITE); //couleur d'ecriture blanche
	BSP_LCD_SetBackColor(LCD_COLOR_BLUE); // fond d'ecran bleu

	//Initialisation de la dalle tactile
	BSP_TS_Init(BSP_LCD_GetXSize(), BSP_LCD_GetYSize());

	//l’activation de l’interruption de réception d’un caractère
	HAL_UART_Receive_IT(&huart1, &rxbuffer, 1);

  /* USER CODE END 2 */

  /* Create the mutex(es) */
  /* definition and creation of myMutex_tp5 */
  osMutexDef(myMutex_tp5);
  myMutex_tp5Handle = osMutexCreate(osMutex(myMutex_tp5));

  /* USER CODE BEGIN RTOS_MUTEX */
	/* add mutexes, ... */
  /* USER CODE END RTOS_MUTEX */

  /* USER CODE BEGIN RTOS_SEMAPHORES */
	/* add semaphores, ... */
  /* USER CODE END RTOS_SEMAPHORES */

  /* USER CODE BEGIN RTOS_TIMERS */
	/* start timers, add new ones, ... */
  /* USER CODE END RTOS_TIMERS */

  /* Create the queue(s) */
  /* definition and creation of QueuePangolin_virus */
  osMessageQDef(QueuePangolin_virus, 16, uint16_t);
  QueuePangolin_virusHandle = osMessageCreate(osMessageQ(QueuePangolin_virus), NULL);

  /* definition and creation of QueueVirus_GammeOver */
  osMessageQDef(QueueVirus_GammeOver, 16, uint16_t);
  QueueVirus_GammeOverHandle = osMessageCreate(osMessageQ(QueueVirus_GammeOver), NULL);

  /* definition and creation of QueueUART_homme */
  osMessageQDef(QueueUART_homme, 16, uint16_t);
  QueueUART_hommeHandle = osMessageCreate(osMessageQ(QueueUART_homme), NULL);

  /* definition and creation of QueueHuminite_Victoire */
  osMessageQDef(QueueHuminite_Victoire, 16, uint16_t);
  QueueHuminite_VictoireHandle = osMessageCreate(osMessageQ(QueueHuminite_Victoire), NULL);

  /* definition and creation of QueueHomme_Virus */
  osMessageQDef(QueueHomme_Virus, 16, uint16_t);
  QueueHomme_VirusHandle = osMessageCreate(osMessageQ(QueueHomme_Virus), NULL);

  /* USER CODE BEGIN RTOS_QUEUES */
	/* add queues, ... */
  /* USER CODE END RTOS_QUEUES */

  /* Create the thread(s) */
  /* definition and creation of Game_Over */
  osThreadDef(Game_Over, Game_over, osPriorityHigh, 0, 1024);
  Game_OverHandle = osThreadCreate(osThread(Game_Over), NULL);

  /* definition and creation of Pangolin */
  osThreadDef(Pangolin, pangolain, osPriorityNormal, 0, 1024);
  PangolinHandle = osThreadCreate(osThread(Pangolin), NULL);

  /* definition and creation of Homme */
  osThreadDef(Homme, homme, osPriorityNormal, 0, 1024);
  HommeHandle = osThreadCreate(osThread(Homme), NULL);

  /* definition and creation of Virus */
  osThreadDef(Virus, virus1, osPriorityNormal, 0, 1024);
  VirusHandle = osThreadCreate(osThread(Virus), NULL);

  /* definition and creation of Vaccin */
  osThreadDef(Vaccin, vaccin, osPriorityNormal, 0, 1024);
  VaccinHandle = osThreadCreate(osThread(Vaccin), NULL);

  /* definition and creation of Victoire */
  osThreadDef(Victoire, victoire, osPriorityHigh, 0, 1024);
  VictoireHandle = osThreadCreate(osThread(Victoire), NULL);

  /* definition and creation of Huminite */
  osThreadDef(Huminite, huminite, osPriorityNormal, 0, 1024);
  HuminiteHandle = osThreadCreate(osThread(Huminite), NULL);

  /* definition and creation of Demarrage */
  osThreadDef(Demarrage, demarrage, osPriorityHigh, 0, 1024);
  DemarrageHandle = osThreadCreate(osThread(Demarrage), NULL);

  /* USER CODE BEGIN RTOS_THREADS */
	/* add threads, ... */
  /* USER CODE END RTOS_THREADS */

  /* Start scheduler */
  osKernelStart();

  /* We should never get here as control is now taken by the scheduler */
  /* Infinite loop */
  /* USER CODE BEGIN WHILE */
	while (1) {

    /* USER CODE END WHILE */

    /* USER CODE BEGIN 3 */
	}
  /* USER CODE END 3 */
}

/**
  * @brief System Clock Configuration
  * @retval None
  */
void SystemClock_Config(void)
{
  RCC_OscInitTypeDef RCC_OscInitStruct = {0};
  RCC_ClkInitTypeDef RCC_ClkInitStruct = {0};
  RCC_PeriphCLKInitTypeDef PeriphClkInitStruct = {0};

  /** Configure LSE Drive Capability
  */
  HAL_PWR_EnableBkUpAccess();
  /** Configure the main internal regulator output voltage
  */
  __HAL_RCC_PWR_CLK_ENABLE();
  __HAL_PWR_VOLTAGESCALING_CONFIG(PWR_REGULATOR_VOLTAGE_SCALE1);
  /** Initializes the RCC Oscillators according to the specified parameters
  * in the RCC_OscInitTypeDef structure.
  */
  RCC_OscInitStruct.OscillatorType = RCC_OSCILLATORTYPE_LSI|RCC_OSCILLATORTYPE_HSE;
  RCC_OscInitStruct.HSEState = RCC_HSE_ON;
  RCC_OscInitStruct.LSIState = RCC_LSI_ON;
  RCC_OscInitStruct.PLL.PLLState = RCC_PLL_ON;
  RCC_OscInitStruct.PLL.PLLSource = RCC_PLLSOURCE_HSE;
  RCC_OscInitStruct.PLL.PLLM = 25;
  RCC_OscInitStruct.PLL.PLLN = 400;
  RCC_OscInitStruct.PLL.PLLP = RCC_PLLP_DIV2;
  RCC_OscInitStruct.PLL.PLLQ = 9;
  if (HAL_RCC_OscConfig(&RCC_OscInitStruct) != HAL_OK)
  {
    Error_Handler();
  }
  /** Activate the Over-Drive mode
  */
  if (HAL_PWREx_EnableOverDrive() != HAL_OK)
  {
    Error_Handler();
  }
  /** Initializes the CPU, AHB and APB buses clocks
  */
  RCC_ClkInitStruct.ClockType = RCC_CLOCKTYPE_HCLK|RCC_CLOCKTYPE_SYSCLK
                              |RCC_CLOCKTYPE_PCLK1|RCC_CLOCKTYPE_PCLK2;
  RCC_ClkInitStruct.SYSCLKSource = RCC_SYSCLKSOURCE_PLLCLK;
  RCC_ClkInitStruct.AHBCLKDivider = RCC_SYSCLK_DIV1;
  RCC_ClkInitStruct.APB1CLKDivider = RCC_HCLK_DIV4;
  RCC_ClkInitStruct.APB2CLKDivider = RCC_HCLK_DIV2;

  if (HAL_RCC_ClockConfig(&RCC_ClkInitStruct, FLASH_LATENCY_6) != HAL_OK)
  {
    Error_Handler();
  }
  PeriphClkInitStruct.PeriphClockSelection = RCC_PERIPHCLK_LTDC|RCC_PERIPHCLK_RTC
                              |RCC_PERIPHCLK_USART1|RCC_PERIPHCLK_I2C3
                              |RCC_PERIPHCLK_CLK48;
  PeriphClkInitStruct.PLLSAI.PLLSAIN = 384;
  PeriphClkInitStruct.PLLSAI.PLLSAIR = 5;
  PeriphClkInitStruct.PLLSAI.PLLSAIQ = 2;
  PeriphClkInitStruct.PLLSAI.PLLSAIP = RCC_PLLSAIP_DIV8;
  PeriphClkInitStruct.PLLSAIDivQ = 1;
  PeriphClkInitStruct.PLLSAIDivR = RCC_PLLSAIDIVR_8;
  PeriphClkInitStruct.RTCClockSelection = RCC_RTCCLKSOURCE_LSI;
  PeriphClkInitStruct.Usart1ClockSelection = RCC_USART1CLKSOURCE_PCLK2;
  PeriphClkInitStruct.I2c3ClockSelection = RCC_I2C3CLKSOURCE_PCLK1;
  PeriphClkInitStruct.Clk48ClockSelection = RCC_CLK48SOURCE_PLLSAIP;
  if (HAL_RCCEx_PeriphCLKConfig(&PeriphClkInitStruct) != HAL_OK)
  {
    Error_Handler();
  }
}

/**
  * @brief DMA2D Initialization Function
  * @param None
  * @retval None
  */
static void MX_DMA2D_Init(void)
{

  /* USER CODE BEGIN DMA2D_Init 0 */

  /* USER CODE END DMA2D_Init 0 */

  /* USER CODE BEGIN DMA2D_Init 1 */

  /* USER CODE END DMA2D_Init 1 */
  hdma2d.Instance = DMA2D;
  hdma2d.Init.Mode = DMA2D_M2M;
  hdma2d.Init.ColorMode = DMA2D_OUTPUT_ARGB8888;
  hdma2d.Init.OutputOffset = 0;
  hdma2d.LayerCfg[1].InputOffset = 0;
  hdma2d.LayerCfg[1].InputColorMode = DMA2D_INPUT_ARGB8888;
  hdma2d.LayerCfg[1].AlphaMode = DMA2D_NO_MODIF_ALPHA;
  hdma2d.LayerCfg[1].InputAlpha = 0;
  if (HAL_DMA2D_Init(&hdma2d) != HAL_OK)
  {
    Error_Handler();
  }
  if (HAL_DMA2D_ConfigLayer(&hdma2d, 1) != HAL_OK)
  {
    Error_Handler();
  }
  /* USER CODE BEGIN DMA2D_Init 2 */

  /* USER CODE END DMA2D_Init 2 */

}

/**
  * @brief I2C3 Initialization Function
  * @param None
  * @retval None
  */
static void MX_I2C3_Init(void)
{

  /* USER CODE BEGIN I2C3_Init 0 */

  /* USER CODE END I2C3_Init 0 */

  /* USER CODE BEGIN I2C3_Init 1 */

  /* USER CODE END I2C3_Init 1 */
  hi2c3.Instance = I2C3;
  hi2c3.Init.Timing = 0x00C0EAFF;
  hi2c3.Init.OwnAddress1 = 0;
  hi2c3.Init.AddressingMode = I2C_ADDRESSINGMODE_7BIT;
  hi2c3.Init.DualAddressMode = I2C_DUALADDRESS_DISABLE;
  hi2c3.Init.OwnAddress2 = 0;
  hi2c3.Init.OwnAddress2Masks = I2C_OA2_NOMASK;
  hi2c3.Init.GeneralCallMode = I2C_GENERALCALL_DISABLE;
  hi2c3.Init.NoStretchMode = I2C_NOSTRETCH_DISABLE;
  if (HAL_I2C_Init(&hi2c3) != HAL_OK)
  {
    Error_Handler();
  }
  /** Configure Analogue filter
  */
  if (HAL_I2CEx_ConfigAnalogFilter(&hi2c3, I2C_ANALOGFILTER_ENABLE) != HAL_OK)
  {
    Error_Handler();
  }
  /** Configure Digital filter
  */
  if (HAL_I2CEx_ConfigDigitalFilter(&hi2c3, 0) != HAL_OK)
  {
    Error_Handler();
  }
  /* USER CODE BEGIN I2C3_Init 2 */

  /* USER CODE END I2C3_Init 2 */

}

/**
  * @brief LTDC Initialization Function
  * @param None
  * @retval None
  */
static void MX_LTDC_Init(void)
{

  /* USER CODE BEGIN LTDC_Init 0 */

  /* USER CODE END LTDC_Init 0 */

  LTDC_LayerCfgTypeDef pLayerCfg = {0};

  /* USER CODE BEGIN LTDC_Init 1 */

  /* USER CODE END LTDC_Init 1 */
  hltdc.Instance = LTDC;
  hltdc.Init.HSPolarity = LTDC_HSPOLARITY_AL;
  hltdc.Init.VSPolarity = LTDC_VSPOLARITY_AL;
  hltdc.Init.DEPolarity = LTDC_DEPOLARITY_AL;
  hltdc.Init.PCPolarity = LTDC_PCPOLARITY_IPC;
  hltdc.Init.HorizontalSync = 40;
  hltdc.Init.VerticalSync = 9;
  hltdc.Init.AccumulatedHBP = 53;
  hltdc.Init.AccumulatedVBP = 11;
  hltdc.Init.AccumulatedActiveW = 533;
  hltdc.Init.AccumulatedActiveH = 283;
  hltdc.Init.TotalWidth = 565;
  hltdc.Init.TotalHeigh = 285;
  hltdc.Init.Backcolor.Blue = 0;
  hltdc.Init.Backcolor.Green = 0;
  hltdc.Init.Backcolor.Red = 0;
  if (HAL_LTDC_Init(&hltdc) != HAL_OK)
  {
    Error_Handler();
  }
  pLayerCfg.WindowX0 = 0;
  pLayerCfg.WindowX1 = 480;
  pLayerCfg.WindowY0 = 0;
  pLayerCfg.WindowY1 = 272;
  pLayerCfg.PixelFormat = LTDC_PIXEL_FORMAT_RGB565;
  pLayerCfg.Alpha = 255;
  pLayerCfg.Alpha0 = 0;
  pLayerCfg.BlendingFactor1 = LTDC_BLENDING_FACTOR1_PAxCA;
  pLayerCfg.BlendingFactor2 = LTDC_BLENDING_FACTOR2_PAxCA;
  pLayerCfg.FBStartAdress = 0xC0000000;
  pLayerCfg.ImageWidth = 480;
  pLayerCfg.ImageHeight = 272;
  pLayerCfg.Backcolor.Blue = 0;
  pLayerCfg.Backcolor.Green = 0;
  pLayerCfg.Backcolor.Red = 0;
  if (HAL_LTDC_ConfigLayer(&hltdc, &pLayerCfg, 0) != HAL_OK)
  {
    Error_Handler();
  }
  /* USER CODE BEGIN LTDC_Init 2 */

  /* USER CODE END LTDC_Init 2 */

}

/**
  * @brief RNG Initialization Function
  * @param None
  * @retval None
  */
static void MX_RNG_Init(void)
{

  /* USER CODE BEGIN RNG_Init 0 */

  /* USER CODE END RNG_Init 0 */

  /* USER CODE BEGIN RNG_Init 1 */

  /* USER CODE END RNG_Init 1 */
  hrng.Instance = RNG;
  if (HAL_RNG_Init(&hrng) != HAL_OK)
  {
    Error_Handler();
  }
  /* USER CODE BEGIN RNG_Init 2 */

  /* USER CODE END RNG_Init 2 */

}

/**
  * @brief RTC Initialization Function
  * @param None
  * @retval None
  */
static void MX_RTC_Init(void)
{

  /* USER CODE BEGIN RTC_Init 0 */

  /* USER CODE END RTC_Init 0 */

  RTC_TimeTypeDef sTime = {0};
  RTC_DateTypeDef sDate = {0};
  RTC_AlarmTypeDef sAlarm = {0};

  /* USER CODE BEGIN RTC_Init 1 */

  /* USER CODE END RTC_Init 1 */
  /** Initialize RTC Only
  */
  hrtc.Instance = RTC;
  hrtc.Init.HourFormat = RTC_HOURFORMAT_24;
  hrtc.Init.AsynchPrediv = 127;
  hrtc.Init.SynchPrediv = 255;
  hrtc.Init.OutPut = RTC_OUTPUT_DISABLE;
  hrtc.Init.OutPutPolarity = RTC_OUTPUT_POLARITY_HIGH;
  hrtc.Init.OutPutType = RTC_OUTPUT_TYPE_OPENDRAIN;
  if (HAL_RTC_Init(&hrtc) != HAL_OK)
  {
    Error_Handler();
  }

  /* USER CODE BEGIN Check_RTC_BKUP */

  /* USER CODE END Check_RTC_BKUP */

  /** Initialize RTC and set the Time and Date
  */
  sTime.Hours = 0x0;
  sTime.Minutes = 0x0;
  sTime.Seconds = 0x0;
  sTime.DayLightSaving = RTC_DAYLIGHTSAVING_NONE;
  sTime.StoreOperation = RTC_STOREOPERATION_RESET;
  if (HAL_RTC_SetTime(&hrtc, &sTime, RTC_FORMAT_BCD) != HAL_OK)
  {
    Error_Handler();
  }
  sDate.WeekDay = RTC_WEEKDAY_MONDAY;
  sDate.Month = RTC_MONTH_JANUARY;
  sDate.Date = 0x1;
  sDate.Year = 0x0;
  if (HAL_RTC_SetDate(&hrtc, &sDate, RTC_FORMAT_BCD) != HAL_OK)
  {
    Error_Handler();
  }
  /** Enable the Alarm A
  */
  sAlarm.AlarmTime.Hours = 0x0;
  sAlarm.AlarmTime.Minutes = 0x0;
  sAlarm.AlarmTime.Seconds = 0x0;
  sAlarm.AlarmTime.SubSeconds = 0x0;
  sAlarm.AlarmTime.DayLightSaving = RTC_DAYLIGHTSAVING_NONE;
  sAlarm.AlarmTime.StoreOperation = RTC_STOREOPERATION_RESET;
  sAlarm.AlarmMask = RTC_ALARMMASK_NONE;
  sAlarm.AlarmSubSecondMask = RTC_ALARMSUBSECONDMASK_ALL;
  sAlarm.AlarmDateWeekDaySel = RTC_ALARMDATEWEEKDAYSEL_DATE;
  sAlarm.AlarmDateWeekDay = 0x1;
  sAlarm.Alarm = RTC_ALARM_A;
  if (HAL_RTC_SetAlarm(&hrtc, &sAlarm, RTC_FORMAT_BCD) != HAL_OK)
  {
    Error_Handler();
  }
  /** Enable the Alarm B
  */
  sAlarm.Alarm = RTC_ALARM_B;
  if (HAL_RTC_SetAlarm(&hrtc, &sAlarm, RTC_FORMAT_BCD) != HAL_OK)
  {
    Error_Handler();
  }
  /** Enable the TimeStamp
  */
  if (HAL_RTCEx_SetTimeStamp(&hrtc, RTC_TIMESTAMPEDGE_RISING, RTC_TIMESTAMPPIN_POS1) != HAL_OK)
  {
    Error_Handler();
  }
  /* USER CODE BEGIN RTC_Init 2 */

  /* USER CODE END RTC_Init 2 */

}

/**
  * @brief TIM1 Initialization Function
  * @param None
  * @retval None
  */
static void MX_TIM1_Init(void)
{

  /* USER CODE BEGIN TIM1_Init 0 */

  /* USER CODE END TIM1_Init 0 */

  TIM_ClockConfigTypeDef sClockSourceConfig = {0};
  TIM_MasterConfigTypeDef sMasterConfig = {0};

  /* USER CODE BEGIN TIM1_Init 1 */

  /* USER CODE END TIM1_Init 1 */
  htim1.Instance = TIM1;
  htim1.Init.Prescaler = 0;
  htim1.Init.CounterMode = TIM_COUNTERMODE_UP;
  htim1.Init.Period = 65535;
  htim1.Init.ClockDivision = TIM_CLOCKDIVISION_DIV1;
  htim1.Init.RepetitionCounter = 0;
  htim1.Init.AutoReloadPreload = TIM_AUTORELOAD_PRELOAD_DISABLE;
  if (HAL_TIM_Base_Init(&htim1) != HAL_OK)
  {
    Error_Handler();
  }
  sClockSourceConfig.ClockSource = TIM_CLOCKSOURCE_INTERNAL;
  if (HAL_TIM_ConfigClockSource(&htim1, &sClockSourceConfig) != HAL_OK)
  {
    Error_Handler();
  }
  sMasterConfig.MasterOutputTrigger = TIM_TRGO_RESET;
  sMasterConfig.MasterOutputTrigger2 = TIM_TRGO2_RESET;
  sMasterConfig.MasterSlaveMode = TIM_MASTERSLAVEMODE_DISABLE;
  if (HAL_TIMEx_MasterConfigSynchronization(&htim1, &sMasterConfig) != HAL_OK)
  {
    Error_Handler();
  }
  /* USER CODE BEGIN TIM1_Init 2 */

  /* USER CODE END TIM1_Init 2 */

}

/**
  * @brief USART1 Initialization Function
  * @param None
  * @retval None
  */
static void MX_USART1_UART_Init(void)
{

  /* USER CODE BEGIN USART1_Init 0 */

  /* USER CODE END USART1_Init 0 */

  /* USER CODE BEGIN USART1_Init 1 */

  /* USER CODE END USART1_Init 1 */
  huart1.Instance = USART1;
  huart1.Init.BaudRate = 115200;
  huart1.Init.WordLength = UART_WORDLENGTH_8B;
  huart1.Init.StopBits = UART_STOPBITS_1;
  huart1.Init.Parity = UART_PARITY_NONE;
  huart1.Init.Mode = UART_MODE_TX_RX;
  huart1.Init.HwFlowCtl = UART_HWCONTROL_NONE;
  huart1.Init.OverSampling = UART_OVERSAMPLING_16;
  huart1.Init.OneBitSampling = UART_ONE_BIT_SAMPLE_DISABLE;
  huart1.AdvancedInit.AdvFeatureInit = UART_ADVFEATURE_NO_INIT;
  if (HAL_UART_Init(&huart1) != HAL_OK)
  {
    Error_Handler();
  }
  /* USER CODE BEGIN USART1_Init 2 */

  /* USER CODE END USART1_Init 2 */

}

/* FMC initialization function */
static void MX_FMC_Init(void)
{

  /* USER CODE BEGIN FMC_Init 0 */

  /* USER CODE END FMC_Init 0 */

  FMC_SDRAM_TimingTypeDef SdramTiming = {0};

  /* USER CODE BEGIN FMC_Init 1 */

  /* USER CODE END FMC_Init 1 */

  /** Perform the SDRAM1 memory initialization sequence
  */
  hsdram1.Instance = FMC_SDRAM_DEVICE;
  /* hsdram1.Init */
  hsdram1.Init.SDBank = FMC_SDRAM_BANK1;
  hsdram1.Init.ColumnBitsNumber = FMC_SDRAM_COLUMN_BITS_NUM_8;
  hsdram1.Init.RowBitsNumber = FMC_SDRAM_ROW_BITS_NUM_12;
  hsdram1.Init.MemoryDataWidth = FMC_SDRAM_MEM_BUS_WIDTH_16;
  hsdram1.Init.InternalBankNumber = FMC_SDRAM_INTERN_BANKS_NUM_4;
  hsdram1.Init.CASLatency = FMC_SDRAM_CAS_LATENCY_1;
  hsdram1.Init.WriteProtection = FMC_SDRAM_WRITE_PROTECTION_DISABLE;
  hsdram1.Init.SDClockPeriod = FMC_SDRAM_CLOCK_DISABLE;
  hsdram1.Init.ReadBurst = FMC_SDRAM_RBURST_DISABLE;
  hsdram1.Init.ReadPipeDelay = FMC_SDRAM_RPIPE_DELAY_0;
  /* SdramTiming */
  SdramTiming.LoadToActiveDelay = 16;
  SdramTiming.ExitSelfRefreshDelay = 16;
  SdramTiming.SelfRefreshTime = 16;
  SdramTiming.RowCycleDelay = 16;
  SdramTiming.WriteRecoveryTime = 16;
  SdramTiming.RPDelay = 16;
  SdramTiming.RCDDelay = 16;

  if (HAL_SDRAM_Init(&hsdram1, &SdramTiming) != HAL_OK)
  {
    Error_Handler( );
  }

  /* USER CODE BEGIN FMC_Init 2 */

  /* USER CODE END FMC_Init 2 */
}

/**
  * @brief GPIO Initialization Function
  * @param None
  * @retval None
  */
static void MX_GPIO_Init(void)
{
  GPIO_InitTypeDef GPIO_InitStruct = {0};

  /* GPIO Ports Clock Enable */
  __HAL_RCC_GPIOE_CLK_ENABLE();
  __HAL_RCC_GPIOB_CLK_ENABLE();
  __HAL_RCC_GPIOA_CLK_ENABLE();
  __HAL_RCC_GPIOG_CLK_ENABLE();
  __HAL_RCC_GPIOJ_CLK_ENABLE();
  __HAL_RCC_GPIOD_CLK_ENABLE();
  __HAL_RCC_GPIOI_CLK_ENABLE();
  __HAL_RCC_GPIOK_CLK_ENABLE();
  __HAL_RCC_GPIOC_CLK_ENABLE();
  __HAL_RCC_GPIOF_CLK_ENABLE();
  __HAL_RCC_GPIOH_CLK_ENABLE();

  /*Configure GPIO pin Output Level */
  HAL_GPIO_WritePin(OTG_FS_PowerSwitchOn_GPIO_Port, OTG_FS_PowerSwitchOn_Pin, GPIO_PIN_SET);

  /*Configure GPIO pin Output Level */
  HAL_GPIO_WritePin(LCD_BL_CTRL_GPIO_Port, LCD_BL_CTRL_Pin, GPIO_PIN_SET);

  /*Configure GPIO pin Output Level */
  HAL_GPIO_WritePin(LED1_GPIO_Port, LED1_Pin, GPIO_PIN_RESET);

  /*Configure GPIO pin Output Level */
  HAL_GPIO_WritePin(LCD_DISP_GPIO_Port, LCD_DISP_Pin, GPIO_PIN_SET);

  /*Configure GPIO pin Output Level */
  HAL_GPIO_WritePin(EXT_RST_GPIO_Port, EXT_RST_Pin, GPIO_PIN_RESET);

  /*Configure GPIO pin : PE3 */
  GPIO_InitStruct.Pin = GPIO_PIN_3;
  GPIO_InitStruct.Mode = GPIO_MODE_INPUT;
  GPIO_InitStruct.Pull = GPIO_NOPULL;
  HAL_GPIO_Init(GPIOE, &GPIO_InitStruct);

  /*Configure GPIO pins : ARDUINO_SCL_D15_Pin ARDUINO_SDA_D14_Pin */
  GPIO_InitStruct.Pin = ARDUINO_SCL_D15_Pin|ARDUINO_SDA_D14_Pin;
  GPIO_InitStruct.Mode = GPIO_MODE_AF_OD;
  GPIO_InitStruct.Pull = GPIO_PULLUP;
  GPIO_InitStruct.Speed = GPIO_SPEED_FREQ_LOW;
  GPIO_InitStruct.Alternate = GPIO_AF4_I2C1;
  HAL_GPIO_Init(GPIOB, &GPIO_InitStruct);

  /*Configure GPIO pins : ULPI_D7_Pin ULPI_D6_Pin ULPI_D5_Pin ULPI_D2_Pin
                           ULPI_D1_Pin ULPI_D4_Pin */
  GPIO_InitStruct.Pin = ULPI_D7_Pin|ULPI_D6_Pin|ULPI_D5_Pin|ULPI_D2_Pin
                          |ULPI_D1_Pin|ULPI_D4_Pin;
  GPIO_InitStruct.Mode = GPIO_MODE_AF_PP;
  GPIO_InitStruct.Pull = GPIO_NOPULL;
  GPIO_InitStruct.Speed = GPIO_SPEED_FREQ_VERY_HIGH;
  GPIO_InitStruct.Alternate = GPIO_AF10_OTG_HS;
  HAL_GPIO_Init(GPIOB, &GPIO_InitStruct);

  /*Configure GPIO pin : PB4 */
  GPIO_InitStruct.Pin = GPIO_PIN_4;
  GPIO_InitStruct.Mode = GPIO_MODE_AF_PP;
  GPIO_InitStruct.Pull = GPIO_NOPULL;
  GPIO_InitStruct.Speed = GPIO_SPEED_FREQ_LOW;
  GPIO_InitStruct.Alternate = GPIO_AF2_TIM3;
  HAL_GPIO_Init(GPIOB, &GPIO_InitStruct);

  /*Configure GPIO pin : OTG_FS_VBUS_Pin */
  GPIO_InitStruct.Pin = OTG_FS_VBUS_Pin;
  GPIO_InitStruct.Mode = GPIO_MODE_INPUT;
  GPIO_InitStruct.Pull = GPIO_NOPULL;
  HAL_GPIO_Init(OTG_FS_VBUS_GPIO_Port, &GPIO_InitStruct);

  /*Configure GPIO pin : Audio_INT_Pin */
  GPIO_InitStruct.Pin = Audio_INT_Pin;
  GPIO_InitStruct.Mode = GPIO_MODE_EVT_RISING;
  GPIO_InitStruct.Pull = GPIO_NOPULL;
  HAL_GPIO_Init(Audio_INT_GPIO_Port, &GPIO_InitStruct);

  /*Configure GPIO pin : OTG_FS_PowerSwitchOn_Pin */
  GPIO_InitStruct.Pin = OTG_FS_PowerSwitchOn_Pin;
  GPIO_InitStruct.Mode = GPIO_MODE_OUTPUT_PP;
  GPIO_InitStruct.Pull = GPIO_NOPULL;
  GPIO_InitStruct.Speed = GPIO_SPEED_FREQ_LOW;
  HAL_GPIO_Init(OTG_FS_PowerSwitchOn_GPIO_Port, &GPIO_InitStruct);

  /*Configure GPIO pin : PI2 */
  GPIO_InitStruct.Pin = GPIO_PIN_2;
  GPIO_InitStruct.Mode = GPIO_MODE_AF_PP;
  GPIO_InitStruct.Pull = GPIO_NOPULL;
  GPIO_InitStruct.Speed = GPIO_SPEED_FREQ_LOW;
  GPIO_InitStruct.Alternate = GPIO_AF3_TIM8;
  HAL_GPIO_Init(GPIOI, &GPIO_InitStruct);

  /*Configure GPIO pin : uSD_Detect_Pin */
  GPIO_InitStruct.Pin = uSD_Detect_Pin;
  GPIO_InitStruct.Mode = GPIO_MODE_INPUT;
  GPIO_InitStruct.Pull = GPIO_NOPULL;
  HAL_GPIO_Init(uSD_Detect_GPIO_Port, &GPIO_InitStruct);

  /*Configure GPIO pin : LCD_BL_CTRL_Pin */
  GPIO_InitStruct.Pin = LCD_BL_CTRL_Pin;
  GPIO_InitStruct.Mode = GPIO_MODE_OUTPUT_PP;
  GPIO_InitStruct.Pull = GPIO_NOPULL;
  GPIO_InitStruct.Speed = GPIO_SPEED_FREQ_LOW;
  HAL_GPIO_Init(LCD_BL_CTRL_GPIO_Port, &GPIO_InitStruct);

  /*Configure GPIO pin : OTG_FS_OverCurrent_Pin */
  GPIO_InitStruct.Pin = OTG_FS_OverCurrent_Pin;
  GPIO_InitStruct.Mode = GPIO_MODE_INPUT;
  GPIO_InitStruct.Pull = GPIO_NOPULL;
  HAL_GPIO_Init(OTG_FS_OverCurrent_GPIO_Port, &GPIO_InitStruct);

  /*Configure GPIO pins : TP3_Pin NC2_Pin */
  GPIO_InitStruct.Pin = TP3_Pin|NC2_Pin;
  GPIO_InitStruct.Mode = GPIO_MODE_INPUT;
  GPIO_InitStruct.Pull = GPIO_NOPULL;
  HAL_GPIO_Init(GPIOH, &GPIO_InitStruct);

  /*Configure GPIO pins : LED1_Pin LCD_DISP_Pin */
  GPIO_InitStruct.Pin = LED1_Pin|LCD_DISP_Pin;
  GPIO_InitStruct.Mode = GPIO_MODE_OUTPUT_PP;
  GPIO_InitStruct.Pull = GPIO_NOPULL;
  GPIO_InitStruct.Speed = GPIO_SPEED_FREQ_LOW;
  HAL_GPIO_Init(GPIOI, &GPIO_InitStruct);

  /*Configure GPIO pin : PI0 */
  GPIO_InitStruct.Pin = GPIO_PIN_0;
  GPIO_InitStruct.Mode = GPIO_MODE_AF_PP;
  GPIO_InitStruct.Pull = GPIO_NOPULL;
  GPIO_InitStruct.Speed = GPIO_SPEED_FREQ_VERY_HIGH;
  GPIO_InitStruct.Alternate = GPIO_AF5_SPI2;
  HAL_GPIO_Init(GPIOI, &GPIO_InitStruct);

  /*Configure GPIO pin : bouton_poussoir_Pin */
  GPIO_InitStruct.Pin = bouton_poussoir_Pin;
  GPIO_InitStruct.Mode = GPIO_MODE_IT_RISING;
  GPIO_InitStruct.Pull = GPIO_NOPULL;
  HAL_GPIO_Init(bouton_poussoir_GPIO_Port, &GPIO_InitStruct);

  /*Configure GPIO pin : LCD_INT_Pin */
  GPIO_InitStruct.Pin = LCD_INT_Pin;
  GPIO_InitStruct.Mode = GPIO_MODE_EVT_RISING;
  GPIO_InitStruct.Pull = GPIO_NOPULL;
  HAL_GPIO_Init(LCD_INT_GPIO_Port, &GPIO_InitStruct);

  /*Configure GPIO pins : PC7 PC6 */
  GPIO_InitStruct.Pin = GPIO_PIN_7|GPIO_PIN_6;
  GPIO_InitStruct.Mode = GPIO_MODE_AF_PP;
  GPIO_InitStruct.Pull = GPIO_NOPULL;
  GPIO_InitStruct.Speed = GPIO_SPEED_FREQ_VERY_HIGH;
  GPIO_InitStruct.Alternate = GPIO_AF8_USART6;
  HAL_GPIO_Init(GPIOC, &GPIO_InitStruct);

  /*Configure GPIO pin : ULPI_NXT_Pin */
  GPIO_InitStruct.Pin = ULPI_NXT_Pin;
  GPIO_InitStruct.Mode = GPIO_MODE_AF_PP;
  GPIO_InitStruct.Pull = GPIO_NOPULL;
  GPIO_InitStruct.Speed = GPIO_SPEED_FREQ_VERY_HIGH;
  GPIO_InitStruct.Alternate = GPIO_AF10_OTG_HS;
  HAL_GPIO_Init(ULPI_NXT_GPIO_Port, &GPIO_InitStruct);

  /*Configure GPIO pin : PF7 */
  GPIO_InitStruct.Pin = GPIO_PIN_7;
  GPIO_InitStruct.Mode = GPIO_MODE_AF_PP;
  GPIO_InitStruct.Pull = GPIO_NOPULL;
  GPIO_InitStruct.Speed = GPIO_SPEED_FREQ_VERY_HIGH;
  GPIO_InitStruct.Alternate = GPIO_AF8_UART7;
  HAL_GPIO_Init(GPIOF, &GPIO_InitStruct);

  /*Configure GPIO pins : PF10 PF9 PF8 */
  GPIO_InitStruct.Pin = GPIO_PIN_10|GPIO_PIN_9|GPIO_PIN_8;
  GPIO_InitStruct.Mode = GPIO_MODE_ANALOG;
  GPIO_InitStruct.Pull = GPIO_NOPULL;
  HAL_GPIO_Init(GPIOF, &GPIO_InitStruct);

  /*Configure GPIO pins : ULPI_STP_Pin ULPI_DIR_Pin */
  GPIO_InitStruct.Pin = ULPI_STP_Pin|ULPI_DIR_Pin;
  GPIO_InitStruct.Mode = GPIO_MODE_AF_PP;
  GPIO_InitStruct.Pull = GPIO_NOPULL;
  GPIO_InitStruct.Speed = GPIO_SPEED_FREQ_VERY_HIGH;
  GPIO_InitStruct.Alternate = GPIO_AF10_OTG_HS;
  HAL_GPIO_Init(GPIOC, &GPIO_InitStruct);

  /*Configure GPIO pin : EXT_RST_Pin */
  GPIO_InitStruct.Pin = EXT_RST_Pin;
  GPIO_InitStruct.Mode = GPIO_MODE_OUTPUT_PP;
  GPIO_InitStruct.Pull = GPIO_NOPULL;
  GPIO_InitStruct.Speed = GPIO_SPEED_FREQ_LOW;
  HAL_GPIO_Init(EXT_RST_GPIO_Port, &GPIO_InitStruct);

  /*Configure GPIO pin : RMII_RXER_Pin */
  GPIO_InitStruct.Pin = RMII_RXER_Pin;
  GPIO_InitStruct.Mode = GPIO_MODE_INPUT;
  GPIO_InitStruct.Pull = GPIO_NOPULL;
  HAL_GPIO_Init(RMII_RXER_GPIO_Port, &GPIO_InitStruct);

  /*Configure GPIO pins : PA0 PA4 */
  GPIO_InitStruct.Pin = GPIO_PIN_0|GPIO_PIN_4;
  GPIO_InitStruct.Mode = GPIO_MODE_ANALOG;
  GPIO_InitStruct.Pull = GPIO_NOPULL;
  HAL_GPIO_Init(GPIOA, &GPIO_InitStruct);

  /*Configure GPIO pins : ULPI_CLK_Pin ULPI_D0_Pin */
  GPIO_InitStruct.Pin = ULPI_CLK_Pin|ULPI_D0_Pin;
  GPIO_InitStruct.Mode = GPIO_MODE_AF_PP;
  GPIO_InitStruct.Pull = GPIO_NOPULL;
  GPIO_InitStruct.Speed = GPIO_SPEED_FREQ_VERY_HIGH;
  GPIO_InitStruct.Alternate = GPIO_AF10_OTG_HS;
  HAL_GPIO_Init(GPIOA, &GPIO_InitStruct);

  /*Configure GPIO pins : PB14 PB15 */
  GPIO_InitStruct.Pin = GPIO_PIN_14|GPIO_PIN_15;
  GPIO_InitStruct.Mode = GPIO_MODE_AF_PP;
  GPIO_InitStruct.Pull = GPIO_NOPULL;
  GPIO_InitStruct.Speed = GPIO_SPEED_FREQ_VERY_HIGH;
  GPIO_InitStruct.Alternate = GPIO_AF5_SPI2;
  HAL_GPIO_Init(GPIOB, &GPIO_InitStruct);

  /* EXTI interrupt init*/
  HAL_NVIC_SetPriority(EXTI15_10_IRQn, 8, 0);
  HAL_NVIC_EnableIRQ(EXTI15_10_IRQn);

}

/* USER CODE BEGIN 4 */

void HAL_UART_RxCpltCallback(UART_HandleTypeDef *huart) {
	uint8_t Message = 0;
	//allumer la led1 si on envoie 'a'
	if (rxbuffer == 'a')
		HAL_GPIO_WritePin(LED1_GPIO_Port, LED1_Pin, 1);
//eteindre la led1 si on envoie 'e'
	if (rxbuffer == 'e')
		HAL_GPIO_WritePin(LED1_GPIO_Port, LED1_Pin, 0);
// si on envoie 'd' ou 's'
	if ((rxbuffer == 'd') || (rxbuffer == 's') || (rxbuffer == 'r')
			|| (rxbuffer == 'f')) {
		//envoie du message à la tache homme
		Message = rxbuffer;
		xQueueSendFromISR(QueueUART_hommeHandle, &Message, 0);
	}

	//On relance l’interruption
	HAL_UART_Receive_IT(&huart1, &rxbuffer, 1);
}
/* USER CODE END 4 */

/* USER CODE BEGIN Header_Game_over */
/**
 * @brief  Function implementing the Game_Over thread.
 * @param  argument: Not used
 * @retval None
 */
/* USER CODE END Header_Game_over */
void Game_over(void const * argument)
{
  /* USER CODE BEGIN 5 */
	char text1[50] = { };
	uint8_t message = 0;
	static TS_StateTypeDef TS_State;
	BSP_TS_GetState(&TS_State);

	//si on reçoit un message de la tache virus
	if (xQueueReceive(QueueVirus_GammeOverHandle, &message, portMAX_DELAY)) {
		//on tie toutes les taches
		vTaskDelete(VictoireHandle);
		vTaskDelete(PangolinHandle);
		vTaskDelete(VirusHandle);
		vTaskDelete(HommeHandle);
		vTaskDelete(HuminiteHandle);
		vTaskDelete(VaccinHandle);
	}
	/* Infinite loop */
	for (;;) {
//affichage du message "GAME OVER" clignotant
		sprintf(text1, "GAME OVER"); // affectation du time dans text1
		xSemaphoreTake(myMutex_tp5Handle, portMAX_DELAY); // Mutex prend la main
		BSP_LCD_SetFont(&Font24); //police = 24
		BSP_LCD_SetTextColor(LCD_COLOR_RED); //couleur d'ecriture rouge
		BSP_LCD_SetBackColor(LCD_COLOR_BLUE); // fond d'ecran bleu
		BSP_LCD_DisplayStringAt(50, 100, (uint8_t*) text1, CENTER_MODE);
		xSemaphoreGive(myMutex_tp5Handle); // Mutex laisse la main
		osDelay(500); //attend 100 ms

		sprintf(text1, "GAME OVER"); // affectation du time dans text1
		xSemaphoreTake(myMutex_tp5Handle, portMAX_DELAY); // Mutex prend la main
		BSP_LCD_SetFont(&Font24);
		BSP_LCD_SetTextColor(LCD_COLOR_YELLOW); //couleur d'ecriture jaune
		BSP_LCD_SetBackColor(LCD_COLOR_BLUE); // fond d'ecran bleu
		BSP_LCD_DisplayStringAt(50, 100, (uint8_t*) text1, CENTER_MODE); //affichage au centre du lcd
		xSemaphoreGive(myMutex_tp5Handle); // Mutex laisse la main
		osDelay(500); //attend 500 ms
	}
  /* USER CODE END 5 */
}

/* USER CODE BEGIN Header_pangolain */
/**
 * @brief Function implementing the Pangolain thread.
 * @param argument: Not used
 * @retval None
 */
/* USER CODE END Header_pangolain */
void pangolain(void const * argument)
{
  /* USER CODE BEGIN pangolain */
	uint8_t message1 = 0, vitesse = 1;
	/* Infinite loop */
	for (;;) {
//tant que le pangolin se deplasse à droite et qu'iln'a pas attaint la bordure du lcd
		while ((Xpangolain != 425) && (vitesse == 1)) {
			//si Xpangolain appartient à (0,100, 150,200,250,300,350,400,450)
			if ((Xpangolain == 0) || (Xpangolain == 50) || (Xpangolain == 100)
					|| (Xpangolain == 150) || (Xpangolain == 200)
					|| (Xpangolain == 250) || (Xpangolain == 300)
					|| (Xpangolain == 350) || (Xpangolain == 400)
					|| (Xpangolain == 450)) {
				//on envoie un message à la tache virus
				xQueueSend(QueuePangolin_virusHandle, &message1, 0);
			}

			//on trace le pangolin
			xSemaphoreTake(myMutex_tp5Handle, portMAX_DELAY); // Mutex prend la main
			BSP_LCD_DrawBitmap(Xpangolain, Ypangolain, (uint8_t*) pangolin_bmp); //bmp 50*25
			xSemaphoreGive(myMutex_tp5Handle); // Mutex laisse la main
			osDelay(100); //on attend 100 ms
			//on supprime la trace du pangolin
			xSemaphoreTake(myMutex_tp5Handle, portMAX_DELAY); // Mutex prend la main
			BSP_LCD_SetTextColor(0); //couleur d'ecriture transparente
			BSP_LCD_FillRect(Xpangolain, Ypangolain, 50, 25); //rectangle 50*25
			xSemaphoreGive(myMutex_tp5Handle); // Mutex laisse la main

			// on incremente Xpangolain de 5
			Xpangolain += 5; //ecriture dans le tableau noir
		}
		//on se depplace à gauche
		vitesse = -1;

		//tant que le pangolin se deplasse à gauche et qu'iln'a pas attaint la bordure du lcd
		while ((Xpangolain >= 5) && (vitesse != 1)) {

			xSemaphoreTake(myMutex_tp5Handle, portMAX_DELAY); // Mutex prend la main
			BSP_LCD_DrawBitmap(Xpangolain, Ypangolain, (uint8_t*) pangolin_bmp); //bmp 50*25
			xSemaphoreGive(myMutex_tp5Handle); // Mutex laisse la main
			osDelay(100); //on attend 100 ms

			//on supprime la trace de l'ancien pangolin
			xSemaphoreTake(myMutex_tp5Handle, portMAX_DELAY); // Mutex prend la main
			BSP_LCD_SetTextColor(0); //couleur d'ecriture transparente
			BSP_LCD_FillRect(Xpangolain, Ypangolain, 50, 25); //rectangle 50*25
			xSemaphoreGive(myMutex_tp5Handle); // Mutex laisse la main

			// on incremente Xpangolain de 5
			Xpangolain -= 5; //ecriture dans le tableau noir
		}
		//on se depplace à droite
		vitesse = 1;
		osDelay(100); //on attend 100 ms
	}
  /* USER CODE END pangolain */
}

/* USER CODE BEGIN Header_homme */
/**
 * @brief Function implementing the Homme thread.
 * @param argument: Not used
 * @retval None
 */
/* USER CODE END Header_homme */
void homme(void const * argument)
{
  /* USER CODE BEGIN homme */
	uint8_t
			Message = 0, // pour contenir le message venant de l'UART
			Message1 = 0, //pour contenir le message envoyé au vaccin pour s'effacer
			Message2 = 0, // pour contenir le messager envoyé à la tache victoire pour se lancer
			etat_touche_d, etat_touche_d_old = 0, etat_touche_s,
			etat_touche_s_old = 0, etat_touche_r, etat_touche_r_old = 0,
			etat_touche_f, etat_touche_f_old = 0;

	uint8_t homme_vaccine = 0, homme_vaccine_old = 0, collision;

	/* Infinite loop */
	for (;;) {

		xSemaphoreTake(myMutex_tp5Handle, portMAX_DELAY); // Mutex prend la main
		BSP_LCD_DrawBitmap(Xhomme, Yhomme, (uint8_t*) bitmap_bmp); //bmp 50*50
		xSemaphoreGive(myMutex_tp5Handle); // Mutex laisse la main
		osDelay(1); //on attend 1 ms

		//on attend en permanance un message de l'UART
		xQueueReceive(QueueUART_hommeHandle, &Message, portMAX_DELAY);
		//si on reçoit 'd', etat_touche_d =1
		etat_touche_d = (Message == 'd');
		//si on reçoit 's', etat_touche_s =1
		etat_touche_s = (Message == 's');
		//si on reçoit 'r', etat_touche_r =1
		etat_touche_r = (Message == 'r');
		//si on reçoit 'f', etat_touche_f =1
		etat_touche_f = (Message == 'f');

		//si on detecte un front montant sur la reception de 'd' et l'homme n'a pas encors atteint la limite du lcd
		if ((etat_touche_d == 1) && (etat_touche_d_old == 0)
				&& (Xhomme <= 420)) {
			xSemaphoreTake(myMutex_tp5Handle, portMAX_DELAY); // Mutex prend la main
			BSP_LCD_DrawBitmap(Xhomme, Yhomme, (uint8_t*) bitmap_bmp);
			xSemaphoreGive(myMutex_tp5Handle); // Mutex laisse la main
			osDelay(1); //on attend 1 ms

			//suppression de la trace de l homme
			xSemaphoreTake(myMutex_tp5Handle, portMAX_DELAY); // Mutex prend la main
			BSP_LCD_SetTextColor(0); //couleur d'ecriture trasparente
			BSP_LCD_FillRect(Xhomme, Yhomme, 50, 50); //traçage du rectangle 50*50
			xSemaphoreGive(myMutex_tp5Handle); // Mutex laisse la main

			//deplacement à droite
			Xhomme += 10; //ecriture dans le tableau noir

			etat_touche_d_old = etat_touche_d; //mise à jour
		}
		etat_touche_d_old = 0; //reinitialisation

		//deplacement à gauche
		//si on detecte un front montant sur la reception de 's' et l'homme n'a pas encors atteint la limite du lcd
		if ((etat_touche_s == 1) && (etat_touche_s_old == 0)
				&& (Xhomme >= 10)) {
			xSemaphoreTake(myMutex_tp5Handle, portMAX_DELAY);
			BSP_LCD_DrawBitmap(Xhomme, Yhomme, (uint8_t*) bitmap_bmp);
			xSemaphoreGive(myMutex_tp5Handle);
			osDelay(1);
			//suppression de la trace de l'homme
			xSemaphoreTake(myMutex_tp5Handle, portMAX_DELAY); // Mutex prend la main
			BSP_LCD_SetTextColor(0); //couleur d'ecriture bleue
			BSP_LCD_FillRect(Xhomme, Yhomme, 50, 50); //traçage du rectangle 50*50
			xSemaphoreGive(myMutex_tp5Handle); // Mutex laisse la main
			Xhomme -= 10;

			etat_touche_s_old = etat_touche_s;
		}
		etat_touche_s_old = 0;

		//deplacement en haut
		//si on detecte un front montant sur la reception de 'r' et l'homme n'a pas encors atteint le vaccin
		if ((etat_touche_r == 1) && (etat_touche_r_old == 0)
				&& (Yhomme > Yvaccin)) {
			xSemaphoreTake(myMutex_tp5Handle, portMAX_DELAY);
			BSP_LCD_DrawBitmap(Xhomme, Yhomme, (uint8_t*) bitmap_bmp);
			xSemaphoreGive(myMutex_tp5Handle);
			osDelay(1);
			//suppression de la trace de l'homme
			xSemaphoreTake(myMutex_tp5Handle, portMAX_DELAY);
			BSP_LCD_SetTextColor(0);
			BSP_LCD_FillRect(Xhomme, Yhomme, 50, 50);
			xSemaphoreGive(myMutex_tp5Handle);
			Yhomme -= 10;
			etat_touche_r_old = etat_touche_r;
		}
		etat_touche_r_old = 0;

		//deplacement en bas
		//si on detecte un front montant sur la reception de 'f' et l'homme n'a pas encors atteint la limite du lcd
		if ((etat_touche_f == 1) && (etat_touche_f_old == 0)
				&& (Yhomme <= 200)) {
			xSemaphoreTake(myMutex_tp5Handle, portMAX_DELAY);
			BSP_LCD_DrawBitmap(Xhomme, Yhomme, (uint8_t*) bitmap_bmp);
			xSemaphoreGive(myMutex_tp5Handle);
			osDelay(1);
			//suppression de la trace de l'homme
			xSemaphoreTake(myMutex_tp5Handle, portMAX_DELAY);
			BSP_LCD_SetTextColor(0);
			BSP_LCD_FillRect(Xhomme, Yhomme, 50, 50);
			xSemaphoreGive(myMutex_tp5Handle);
			Yhomme += 10;
			etat_touche_f_old = etat_touche_f;
		}
		etat_touche_f_old = 0;

		//si on detecte une collision homme/vaccin
		if ((((Xvaccin > Xhomme) && (Xvaccin <= (Xhomme + 50)))
				|| (((Xvaccin + 50) > Xhomme)
						&& ((Xvaccin + 50) <= (Xhomme + 50))))
				&& (((Yvaccin + 50) > Yhomme)
						&& ((Yvaccin + 50) <= (Yhomme + 50)))) {
			homme_vaccine = 1;
			if (homme_vaccine_old == 0)
				collision = 1;
			else
				collision = 0;
		} else {
			homme_vaccine = 0;

			collision = 0;
			homme_vaccine_old = 0;
		}

		if ((homme_vaccine == 1) && (homme_vaccine_old == 0)
				&& (collision == 1)) {

			huminite_homme += 1;
			xQueueSend(QueueHomme_VirusHandle, &Message1, 0);//message envoyé au vaccin pour s'effacer
			homme_vaccine_old = 1;
		}
		if (huminite_homme > 2) {
			xQueueSend(QueueHuminite_VictoireHandle, &Message2, 0);	//messager envoyé à la tache victoire
		}
	}
  /* USER CODE END homme */
}

/* USER CODE BEGIN Header_virus1 */
/**
 * @brief Function implementing the Virus1 thread.
 * @param argument: Not used
 * @retval None
 */
/* USER CODE END Header_virus1 */
void virus1(void const * argument)
{
  /* USER CODE BEGIN virus1 */
	uint8_t message1 = 0,// pour contenir un message venant de la tache pangolin
			message2 = 0, homme_contamine, homme_contamine_old;

	/* Infinite loop */
	for (;;) {
		homme_contamine = 0;
		homme_contamine_old = 0;
		//attente permanante de reception d'un message de la tache pangolin
		xQueueReceive(QueuePangolin_virusHandle, &message1, portMAX_DELAY);
		//ecriture dans le tableau noir
		Xvirus1 = Xpangolain;
		Yvirus1 = Ypangolain + 30;
		while ((Yvirus1 != 260)) { //tant qu'on n'a pas atteint la limite basse du lcd

			xSemaphoreTake(myMutex_tp5Handle, portMAX_DELAY);
			BSP_LCD_DrawBitmap(Xvirus1, Yvirus1, (uint8_t*) virus1_bmp); //bmp 20*20
			xSemaphoreGive(myMutex_tp5Handle);
			osDelay(100);
			//suppression des traces du virus
			xSemaphoreTake(myMutex_tp5Handle, portMAX_DELAY);
			BSP_LCD_SetTextColor(0);
			BSP_LCD_FillRect(Xvirus1, Yvirus1, 20, 20);
			xSemaphoreGive(myMutex_tp5Handle);
			Yvirus1 += 5; //deplacement en bas

			//si on detecte un collision avec l'homme
			if (((((Xvirus1 + 20) > Xhomme) && (Xvirus1 < (Xhomme + 50)))
					|| ((Xvirus1 > Xhomme) && (Xvirus1 < (Xhomme + 50))))
					&& (((Yvirus1 + 20) >= Yhomme)
							&& ((Yvirus1) <= (Yhomme + 50)))) {
				homme_contamine = 1;
			} else
				homme_contamine = 0;

			if ((huminite_homme >= 0) && (homme_contamine == 1)
					&& (homme_contamine_old == 0)) { //si l'homme a deja pris une dose
				//sa derniere dose sera annulée
				huminite_homme -= 1;
				homme_contamine_old = 1;
				//s'il ne lui reste plus une dose
				if (huminite_homme < 0) {
					xQueueSend(QueueVirus_GammeOverHandle, &message1, 0);//message envoyé à la tache Gameover
				}
			}

		}
		osDelay(1);
		//suppression du virus quant il arrive aux limites inferieures du lcd
		if (Yvirus1 >= 257) {
			xSemaphoreTake(myMutex_tp5Handle, portMAX_DELAY);
			BSP_LCD_SetTextColor(0);
			BSP_LCD_FillRect(Xvirus1, Yvirus1, 20, 20);
			xSemaphoreGive(myMutex_tp5Handle);
		}
		osDelay(1); //on attent 1ms
	}
  /* USER CODE END virus1 */
}

/* USER CODE BEGIN Header_vaccin */
/**
 * @brief Function implementing the Vaccin thread.
 * @param argument: Not used
 * @retval None
 */
/* USER CODE END Header_vaccin */
void vaccin(void const * argument)
{
  /* USER CODE BEGIN vaccin */
	uint32_t rand32;
	uint8_t message1 = 0;
	srand(time(NULL));
	/* Infinite loop */
	for (;;) {
		//appel d'une fonction de generation de nombres aleatoire
		HAL_RNG_GenerateRandomNumber(&hrng, &rand32);
		Xvaccin = rand32 % 425; // donner un abscice aléatoire entre 0 et 425 du vaccin
		Yvaccin = 40 + rand32 % 50;
		if (xQueueReceive(QueueHomme_VirusHandle, &message1, 50)) { //s'il y a reception d'un message de l'homme
			xSemaphoreTake(myMutex_tp5Handle, portMAX_DELAY);
			//le vaccin doit se supprimer
			BSP_LCD_SelectLayer(0);
			BSP_LCD_SetTextColor(LCD_COLOR_BLUE);
			BSP_LCD_FillRect(Xvaccin, Yvaccin, 50, 50);
			BSP_LCD_SelectLayer(1);
			xSemaphoreGive(myMutex_tp5Handle);
			osDelay(50);
		}

		//traçage du vaccin
		xSemaphoreTake(myMutex_tp5Handle, portMAX_DELAY);
		BSP_LCD_SelectLayer(0);
		BSP_LCD_DrawBitmap(Xvaccin, Yvaccin, (uint8_t*) vaccin_bmp); //bmp 50*50
		BSP_LCD_SelectLayer(1);
		xSemaphoreGive(myMutex_tp5Handle);
		osDelay(3000); //on attend 3s
		//suppression du vaccin
		xSemaphoreTake(myMutex_tp5Handle, portMAX_DELAY);
		BSP_LCD_SelectLayer(0);
		BSP_LCD_SetTextColor(LCD_COLOR_BLUE);
		BSP_LCD_FillRect(Xvaccin, Yvaccin, 50, 50);
		BSP_LCD_SelectLayer(1);
		xSemaphoreGive(myMutex_tp5Handle);

	}
  /* USER CODE END vaccin */
}

/* USER CODE BEGIN Header_victoire */
/**
 * @brief Function implementing the Victoire thread.
 * @param argument: Not used
 * @retval None
 */
/* USER CODE END Header_victoire */
void victoire(void const * argument)
{
  /* USER CODE BEGIN victoire */
	char text1[50] = { };
	uint8_t message = 0;

	xQueueReceive(QueueHuminite_VictoireHandle, &message, portMAX_DELAY);
	{
		vTaskDelete(Game_OverHandle);
		vTaskDelete(PangolinHandle);
		vTaskDelete(VirusHandle);
		vTaskDelete(HommeHandle);
		vTaskDelete(HuminiteHandle);
		vTaskDelete(VaccinHandle);
	}
	/* Infinite loop */
	for (;;) {

		sprintf(text1, "VICTOIRE");
		xSemaphoreTake(myMutex_tp5Handle, portMAX_DELAY);
		BSP_LCD_SetFont(&Font24); //police = 24
		BSP_LCD_SetTextColor(LCD_COLOR_RED); //couleur d'ecriture rouge
		BSP_LCD_SetBackColor(LCD_COLOR_BLUE); // fond d'ecran bleu
		BSP_LCD_DisplayStringAt(50, 100, (uint8_t*) text1, CENTER_MODE); //affichage central
		xSemaphoreGive(myMutex_tp5Handle);
		osDelay(500); //attend 500 ms

		sprintf(text1, "VICTOIRE");
		xSemaphoreTake(myMutex_tp5Handle, portMAX_DELAY);
		BSP_LCD_SetFont(&Font24);
		BSP_LCD_SetTextColor(LCD_COLOR_YELLOW); //couleur d'ecriture jaune
		BSP_LCD_SetBackColor(LCD_COLOR_BLUE); // fond d'ecran bleu
		BSP_LCD_DisplayStringAt(50, 100, (uint8_t*) text1, CENTER_MODE);
		xSemaphoreGive(myMutex_tp5Handle);
		osDelay(500); //attend 500 ms

	}
  /* USER CODE END victoire */
}

/* USER CODE BEGIN Header_huminite */
/**
 * @brief Function implementing the Huminite thread.
 * @param argument: Not used
 * @retval None
 */
/* USER CODE END Header_huminite */
void huminite(void const * argument)
{
  /* USER CODE BEGIN huminite */
	char text1[50] = { };
	RTC_TimeTypeDef time;
	RTC_DateTypeDef date;
	uint16_t millisecond;

	/* Infinite loop */
	for (;;) {

		//appels des fonctions pédéfinies pour pouvoir afficher l'heur
		HAL_RTC_GetTime(&hrtc, &time, RTC_FORMAT_BIN);
		HAL_RTC_GetDate(&hrtc, &date, RTC_FORMAT_BIN);
		millisecond = (time.SecondFraction - time.SubSeconds) * 1000
				/ (time.SecondFraction + 1);

		sprintf(text1, " %d : %d :%d", time.Minutes, time.Seconds,
				millisecond); // affectation du time dans text1
		xSemaphoreTake(myMutex_tp5Handle, portMAX_DELAY); // Mutex prend la main
		BSP_LCD_SetTextColor(LCD_COLOR_WHITE); //couleur d'ecriture blanche
		BSP_LCD_SetBackColor(0); // fond d'ecran bleu
		BSP_LCD_DisplayStringAt(0, 0, (uint8_t*) text1, LEFT_MODE); //affichage en haut à gauche du lcd
		xSemaphoreGive(myMutex_tp5Handle); // Mutex laisse la main
		osDelay(1); //attend 1 ms


		//affuchage huminite à droite de l'ecran
		sprintf(text1, "huminite : %d", huminite_homme); // affectation du time dans text1
		xSemaphoreTake(myMutex_tp5Handle, portMAX_DELAY); // Mutex prend la main
		BSP_LCD_SetTextColor(LCD_COLOR_WHITE); //couleur d'ecriture blanche
		BSP_LCD_SetBackColor(0); // fond d'ecran bleu
		BSP_LCD_DisplayStringAt(0, 0, (uint8_t*) text1, RIGHT_MODE); //affichage en haut à gauche du lcd
		xSemaphoreGive(myMutex_tp5Handle); // Mutex laisse la main
		osDelay(1);
	}
  /* USER CODE END huminite */
}

/* USER CODE BEGIN Header_demarrage */
/**
 * @brief Function implementing the Demarrage thread.
 * @param argument: Not used
 * @retval None
 */
/* USER CODE END Header_demarrage */
void demarrage(void const * argument)
{
  /* USER CODE BEGIN demarrage */

	char text1[50] = { };
	char text2[60] = { };
	char text3[100] = { };

	//cette tache cmmence par suspendre toutes les autres taches

	static TS_StateTypeDef TS_State;
	vTaskSuspend(Game_OverHandle);
	vTaskSuspend(VictoireHandle);
	vTaskSuspend(PangolinHandle);
	vTaskSuspend(VirusHandle);
	vTaskSuspend(HommeHandle);
	vTaskSuspend(HuminiteHandle);
	vTaskSuspend(VaccinHandle);

	//Afficher les instruction du jeu
	sprintf(text1, "Toucher l'ecran pour commencer");
	xSemaphoreTake(myMutex_tp5Handle, portMAX_DELAY); // Mutex prend la main
	BSP_LCD_SetFont(&Font12);
	BSP_LCD_SetTextColor(LCD_COLOR_BLACK);
	BSP_LCD_SetBackColor(0);
	BSP_LCD_DisplayStringAtLine(0, (uint8_t*) text1); //affichage en haut à gauche du lcd
	xSemaphoreGive(myMutex_tp5Handle);

	sprintf(text2, "utiliser les touches (d,s,r,f) pour deplacer l'homme");
	xSemaphoreTake(myMutex_tp5Handle, portMAX_DELAY);
	BSP_LCD_SetFont(&Font12);
	BSP_LCD_SetTextColor(LCD_COLOR_BLACK);
	BSP_LCD_SetBackColor(0);
	BSP_LCD_DisplayStringAtLine(5, (uint8_t*) text2); //affichage en haut à gauche du lcd
	xSemaphoreGive(myMutex_tp5Handle);

	sprintf(text3, "si l'homme prend 3 doses successives du vaccin: victoire");
	xSemaphoreTake(myMutex_tp5Handle, portMAX_DELAY);
	BSP_LCD_SetFont(&Font12);
	BSP_LCD_SetTextColor(LCD_COLOR_BLACK);
	BSP_LCD_SetBackColor(0);
	BSP_LCD_DisplayStringAtLine(10, (uint8_t*) text3); //affichage à gauche du lcd
	xSemaphoreGive(myMutex_tp5Handle);
	BSP_TS_Init(BSP_LCD_GetXSize(), BSP_LCD_GetYSize());

	/* Infinite loop */
	for (;;) {
		//si on touche l'ecran le jeu se lance
		BSP_TS_GetState(&TS_State);
		if (TS_State.touchDetected) {
			BSP_LCD_Clear(0);
			vTaskResume(Game_OverHandle);
			vTaskResume(VictoireHandle);
			vTaskResume(PangolinHandle);
			vTaskResume(VirusHandle);
			vTaskResume(HommeHandle);
			vTaskResume(HuminiteHandle);
			vTaskResume(VaccinHandle);

		}
		osDelay(1);
	}
  /* USER CODE END demarrage */
}

 /**
  * @brief  Period elapsed callback in non blocking mode
  * @note   This function is called  when TIM6 interrupt took place, inside
  * HAL_TIM_IRQHandler(). It makes a direct call to HAL_IncTick() to increment
  * a global variable "uwTick" used as application time base.
  * @param  htim : TIM handle
  * @retval None
  */
void HAL_TIM_PeriodElapsedCallback(TIM_HandleTypeDef *htim)
{
  /* USER CODE BEGIN Callback 0 */

  /* USER CODE END Callback 0 */
  if (htim->Instance == TIM6) {
    HAL_IncTick();
  }
  /* USER CODE BEGIN Callback 1 */

  /* USER CODE END Callback 1 */
}

/**
  * @brief  This function is executed in case of error occurrence.
  * @retval None
  */
void Error_Handler(void)
{
  /* USER CODE BEGIN Error_Handler_Debug */
	/* User can add his own implementation to report the HAL error return state */
	__disable_irq();
	while (1) {
	}
  /* USER CODE END Error_Handler_Debug */
}

#ifdef  USE_FULL_ASSERT
/**
  * @brief  Reports the name of the source file and the source line number
  *         where the assert_param error has occurred.
  * @param  file: pointer to the source file name
  * @param  line: assert_param error line source number
  * @retval None
  */
void assert_failed(uint8_t *file, uint32_t line)
{
  /* USER CODE BEGIN 6 */
  /* User can add his own implementation to report the file name and line number,
     ex: printf("Wrong parameters value: file %s on line %d\r\n", file, line) */
  /* USER CODE END 6 */
}
#endif /* USE_FULL_ASSERT */

/************************ (C) COPYRIGHT STMicroelectronics *****END OF FILE****/
