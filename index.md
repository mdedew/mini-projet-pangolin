# Mini-projet du module II: Jeu de pangolin 
Ce projet a été réalisé par **_DEDEW Mohamed Lemine_**, élève en M1 GEII, à l'ENS Paris-Saclay, dans le cadre du module d'Informatique Industrielle. 

L'objectif de ce mini projet est de maitriser le système d'exploitation temps réel *FreeRTOS* sur le microcontrôleur de la carte *STM32*.

Dans ce mini projet, j'ai créé un jeu 2D de **pangolin**. j'ai utilisé dans ce projet une liaison série entre la carte *STM32* et le PC. Pour jouer, il faut donc assurer une liaison série entre la carte et le clavier en assurant le début nominal. On peut utiliser des logiciels pouvant accomplir cette tâche; tels que *Teraterm*.  


## De quoi est constitué ce jeu du pangolin ?
Ce jeu est constitué de:  
  * un **pangolin** se deplaçant en haut de l'écran de façon horizontale, entre les 2 bordures droite et gauche de l'ecran. Ce déplacement 'va et vient' est autonome.  
  
  *  des **virus** lancés par le pangolain à des eendroits bien précis.  
  
  * un **homme** en bas de l'écran, pouvant se déplacer dans toutes le directions.  
  
  * un **vaccin** qui s'affiche périodiquement à des positions alèatoires.  
  


### Comment jouer ?  
Dés que la carte est alimentée, l'écran affiche les instriction du jeu:

![screenshot](img_jeu_1.jpg)  
![screenshot](IMG_0551.jpg)


* Toucher l'écran pour commencer.


* Utiliser les touches '**d**','**s**','**r**' et '**f**', pour deplacer le joueur respectivement à droite, à gauche, en haut, et en bas.
* Si l'homme prend 3 doses successives du vaccin, c'est un **victoire**.
* Si le virus touche l'homme, l'huminité de l'homme decremente de 1.
Si celle-ci devient inferieur ou égale à 0, c'est: **"Game over"**.

* Si l'homme touche le vaccin, son huminité s'incrémente de 1.



