# Liaison série, tâches, schéma d'interaction & ce qui ne fonctionne pas 

## Liaison série
Comme dit dans la page précedente, une liason série avec une telle configuration est necessaire pour que vous puissier jouer.  
Dnans les photos suivantes, je vous montere la configuration necessaire avec le logiciel **Teraterm**  
  

  
  
![screenshot](Capture_1_tera.png)  

![screenshot](Capture_2_tera.png)  









## Les tâches du projet 
Ce projet comporte plusieurs tâches patageant la CPU du microcontrôleur de la carte *STM32*, ainsi que la ressource critique(l'écran **_LCD_**).
  * **Démarrage**  
Cette tâche commence par suspendre toutes les autres tâches, et affiche le message des instructions du jeu vu dans précédement et rentre dans un veil tant que l'écran n'est pas touché. Une fois l'écran est touché, celle-ci se lance en libérant les autres tâches.  


  * **Pangolin**     
  Trace un panglin qui se déplace de façon autonome à gauche et à droite en haut de l'écran et qui envoie des messages périodiques à la tâche *Virus*.  
  ![screenshot](Pangolin.png)  
  
  
  
  * **Virus**  
  Dés que cette tâche reçoit un message de la tâche précedente, elle trace un virus qui se déplace vers le bas. S'il détecte une collision avec l'homme, la valeur de l'huminité de l'homme est retranchée de 1. Si non, le virus s'éfface s'il atteint la bordure inferieur de l'écran. Cette tâche vérifie à tout moment la valeur de l'huminité, si celle-ci est inferieur ou égale à zéro, elle envoie un message à la tâche *Game over*.
  ![screenshot](virus.jpg)  
  
  
  
  * **Homme**  
  Elle trace un home en bas de l'écran. cet homme peut se déplacer via la liason série vers toute les direction.S'il détecte une collision avec le vaccin, la valeur de l'huminité de l'homme est incréméntée de 1. Cette tâche vérifie à tout moment la valeur de l'huminité, si celle-ci est >=2, elle envoie un message à la tâche *Victoire*.  
  ![screenshot](homme.png)  
  
  
  
  * **Vaccin**  
  Cette tâche un vaccin à des coordonnées aléatoires pendant 3s, avant de le supprimer.
  ![screenshot](vaccin.jpg)  
  
  
  
  * **Huminité**  
  Cette tâche affiche la valeur de l'huminité et le timing du jeu.   
  
  
  
  
  * **victoire**  
  Cette tâche se lance quand elle reçoit un message de la tâche *Homme*, elle tue toutes les autres tâches, et affiche un sur l'écran le message clignotant: "*Game Over*"  
  
  
  
  * **Game Over**  
  Cette tâche se lance quand elle reçoit un message de la tâche *Virus*, elle tue toutes les autres tâches, et affiche un sur l'écran le message clignotant: "**Victoire**"  
  
  
##  Schéma d'interaction entre tâches

La photo suivante, malgré sa comlexité, peut nous donner une idée sur les interactions entres ces tâches.
![screenshot](interaction.png)  

##  Ce qui ne fonctionne pas
*Normalent quand l'homme touche le vaccin, ce dernier doit s'éffacer, alors que c'est n'est pas le cas!*




